# LFortran

LFortran is a modern open-source (BSD licensed) interactive Fortran compiler
built on top of LLVM. It can execute user's code interactively to allow
exploratory work (much like Python, MATLAB or Julia) as well as compile to
binaries with the goal to run user's code on modern architectures such as
multi-core CPUs and GPUs.

# Documentation

All documentation, installation instructions, motivation, design, ... is
available at:

https://docs.lfortran.org/

Which is generated using the files in the `doc` directory.
